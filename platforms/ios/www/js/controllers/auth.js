angular.module('mydesignApp')
.controller('authCtrl', ['$window', '$rootScope', '$scope', '$http', '$location',
      function($window, $rootScope, $scope, $http, $location) {
        if ($window.localStorage.getItem('userInfo') !== null && 
            $window.localStorage.getItem('userInfo') !== undefined && 
            $window.localStorage.getItem('userInfo') !== "undefined") {
            $rootScope.user = JSON.parse($window.localStorage.getItem('userInfo')); 
            $rootScope.user.logged = true;
            $rootScope.user.profile_pic = $rootScope.user.profile_pic === undefined 
            ? 'img/user_sample.jpg' : $rootScope.user.profile_pic;
            $location.path('dashboard');
        } else {
            $rootScope.user = { 'logged':false };
        }

      $scope.back = function() { 
         window.history.back();
      };  

      $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

      // FB Login
      $scope.fbLogin = function () {

        console.log('connect');
        var client_id = '141101123273269'; //your App ID or API Key
        var client_secret = 'aa12109c97d76f8564a291c9c80d4595'; //// your App Secret
        var redirect_uri = 'https://www.facebook.com/connect/login_success.html';  //// YOUR CALLBACK URL
        var display = 'touch';
        var authorize_url = "https://graph.facebook.com/v2.0/oauth/authorize?";
        authorize_url += "client_id=" + client_id;
        authorize_url += "&redirect_uri=" + redirect_uri;
        authorize_url += "&display=" + display;
        authorize_url += "&scope=public_profile,email";

        var ref = window.open(authorize_url, '_blank', 'location=yes');
        ref.addEventListener('loadstart', function(event)
        {
            var loc = event.url;
            if(loc.indexOf(redirect_uri + "?") >= 0)
            {
                ref.close();
                var result = loc.split("#")[0];
                console.log(loc);
                login_accessToken = result.split("&")[0].split("=")[1];

                var url = 'https://graph.facebook.com/v2.0/oauth/access_token?';
                    url += 'client_id=' + client_id;
                    url += '&client_secret=' + client_secret;
                    url += '&code=' + login_accessToken;
                    url += '&redirect_uri=' + redirect_uri;

                $http.post(url,null)
                .success(function(data){
                    accessToken = data.access_token;
                    console.log(accessToken);
                    url = "https://graph.facebook.com/v2.5/me?fields=id,name,email&access_token=" + accessToken;
                    $http.get(url).success(function(data){
                      var user = {};
                      var name = data.name.split(" ")
                      user.first_name = name[0];
                      user.last_name = name[1] !== undefined ? name[1] : null;
                      user.email = data.email;
                      user.password = data.id // password for social login is the ID
                      user.profile_pic = 'img/user_sample.jpg';
                      if(!isStoraged(user)) {
                        saveUser(user, true);
                      }else{
                        loadProfile(user);
                      }
                    });
                })
                .error(function(data, status, headers, config) {
                  $scope.showError = true;
                  $scope.message = 'Oops! Something went wrong!';
                  $('#confirmation-window').show(); 
                });
            }
        });
      };

      function isStoraged(user) {
        return user.id !== undefined;
      }

      function loadProfile(user) {
        $window.localStorage['userInfo'] = JSON.stringify(user);
        if(!user.is_social_login){
          $rootScope.profile_pic = user.profile_pic;
          user['profile_pic'] = {};
        }

        $window.localStorage.setItem('userInfo', user);
        $location.path('dashboard');
      }

    $scope.signIn= function(user){
        $http.post(__env.apiUrl + '/login', user)
        .success(function(data, status, headers, config) {
            $rootScope.user = data.payload;
            $rootScope.user.logged = true;
            $rootScope.user.profile_pic = $rootScope.user.profile_pic === undefined 
            ? 'img/user_sample.jpg' : $rootScope.user.profile_pic;
            $window.localStorage.setItem('userInfo', JSON.stringify($rootScope.user));
            $location.path('dashboard');
        })
        .error(function(data, status, headers, config) {
          $scope.showError = true;
          $scope.message = 'Oops! Something went wrong!';
          $('#confirmation-window').show(); 
        });
    }

      $scope.emailSignUp = function(user){
          saveUser(user, false);
      }

      function saveUser(user, is_social_login) {
        var userObj = {
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            password: user.password,
            profile_pic: is_social_login ? user.profile_pic : 'img/user_sample.jpg',
            is_social_login: is_social_login,
            is_subscriber: user.is_subscriber !== undefined ? user.is_subscriber : false
          };

        $http.post(__env.apiUrl + '/user', userObj)
        .success(function(data, status, headers, config) {
            if (data.code === 409) {
                $scope.showError = true;
                $scope.message = data.message;
                $('#confirmation-window').show();
            } else {
                $rootScope.user = data.payload;
                $rootScope.user.logged = true;
                $window.localStorage.setItem('userInfo', JSON.stringify($rootScope.user));
                $location.path('invite');
            }
		})
		.error(function(data, status, headers, config) {
			$scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
		});
      }
}]);
