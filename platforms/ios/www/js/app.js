// App.js
var mydesignApp = angular.module('mydesignApp', 
                    ['ngRoute', 'ngCookies','720kb.socialshare','angular-md5', 'stripe'])
.config(function($routeProvider) { 
     Stripe.setPublishableKey('pk_test_0NUzvThEJz0Ix3m6gLW7d6Vt');
     $routeProvider
        .when('/home', {
                templateUrl: "partials/auth/home.html",
                controller: 'authCtrl'
            })
        .when('/signup', {
                templateUrl: "partials/auth/signup.html",
                controller: 'authCtrl'
            })
        .when('/signin', {
            templateUrl: "partials/auth/signin.html",
            controller: 'authCtrl'
        }) 
        .when('/invite', {
            templateUrl: "partials/invite.html",
            controller: 'dashboardCtrl'
        })
        .when('/notloggeddashboard', {
            templateUrl: "partials/notlogged_dashboard.html",
            controller: 'dashboardCtrl'
        })
        .when('/notlogged', {
            templateUrl: "partials/notlogged.html",
            controller: 'dashboardCtrl'
        })             
        .when('/dashboard', {
            templateUrl: "partials/logged_dashboard.html",
            controller: 'dashboardCtrl'
        })
        .when('/withoutlogin', {
            templateUrl: "partials/withoutlogin.html",
            controller: 'dashboardCtrl'
        })    
        .when('/chooseshape', {
                templateUrl: "partials/shape/choose_shape.html",
                controller: 'shapeCtrl'
            })
        .when('/confirmshape', {
                templateUrl: "partials/shape/confirm_shape.html",
                controller: 'shapeCtrl'
        }) 
        .when('/choosecategory', {
            templateUrl: "partials/category/choose_category.html",
            controller: 'categoryCtrl'
        })  
        .when('/chooseoccassion', {
            templateUrl: "partials/choose_occassion.html",
            controller: 'occasionCtrl'
        })  
        .when('/chooseclothes', {
            templateUrl: "partials/choose_clothes.html",
            controller: 'clothesCtrl'
        })   
        .when('/listfabric', {
            templateUrl: "partials/choose_fabric.html",
            controller: 'fabricCtrl'
        }) 
        .when('/choosefabric', {
            templateUrl: "partials/choose_from_prints.html",
            controller: 'fabricCtrl'
        })    
        .when('/confirmfabric', {
            templateUrl: "partials/confirm_fabric.html",
            controller: 'fabricCtrl'
        })           
        .when('/readytomeasure', {
            templateUrl: "partials/ready_to_measure.html",
            controller: 'fabricCtrl'
        })  
        .when('/sizechart', {
            templateUrl: "partials/size_chart.html",
            controller: 'measureCtrl'
        })              
        .when('/adjustsize', {
            templateUrl: "partials/adjust_measure.html",
            controller: 'measureCtrl'
        })  
        .when('/confirmadjust', {
            templateUrl: "partials/confirm_adjust.html",
            controller: 'measureCtrl'
        })     
        .when('/checkout', {
            templateUrl: "partials/checkout.html",
            controller: 'measureCtrl'
        }) 
        .when('/checkoutlogin', {
            templateUrl: "partials/checkout_login.html",
            controller: 'paymentController'
        })    
        .when('/deliveryaddress', {
            templateUrl: "partials/delivery_address.html",
            controller: 'paymentController'
        })      
        .when('/shippingtype', {
            templateUrl: "partials/shipping_type.html",
            controller: 'paymentController'
        }) 
        .when('/giftoptions', {
            templateUrl: "partials/gift_options.html",
            controller: 'paymentController'
        }) 
        .when('/payment', {
            templateUrl: "partials/payment.html",
            controller: 'paymentController'
        }) 
        .when('/paymentreview', {
            templateUrl: "partials/payment_review.html",
            controller: 'paymentController'
        })   
        .when('/paymentdone', {
            templateUrl: "partials/payment_done.html",
            controller: 'paymentController'
        })  
        .when('/cart', {
            templateUrl: "partials/cart.html",
            controller: 'paymentController'
        })
        .when('/closet', {
            templateUrl: "partials/closet.html",
            controller: 'closetCtrl'
        })                                                                                             
        .when('/shareapp', {
            templateUrl: "partials/share_app.html",
            controller: 'usersCtrl'
        }) 
        .when('/sharedesign', {
            templateUrl: "partials/share_your_design.html",
            controller: 'closetCtrl'
        }) 
        .when('/editprofile', {
            templateUrl: "partials/edit_profile.html",
            controller: 'usersCtrl'
        })
        .when('/forgotpassword', {
            templateUrl: "partials/auth/forgot_password.html",
            controller: 'usersCtrl'
        })
        .when('/passwordsent', {
            templateUrl: "partials/auth/password_sent.html",
            controller: 'usersCtrl'
        })  
        .when('/changepassword', {
            templateUrl: "partials/auth/change_password.html",
            controller: 'usersCtrl'
        }) 
        .when('/purchase', {
            templateUrl: "partials/purchase.html",
            controller: 'purchaseCtrl'
        })  
        .when('/contact', {
            templateUrl: "partials/contact.html",
            controller: 'dashboardCtrl'
        })
        .when('/help', {
            templateUrl: "partials/help.html",
            controller: 'dashboardCtrl'
        })  
        .when('/terms', {
            templateUrl: "partials/terms.html",
            controller: 'dashboardCtrl'
        })       
        .otherwise({
            redirectTo: '/home'
        });
});
