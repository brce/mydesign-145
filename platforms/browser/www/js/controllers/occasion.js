angular.module('mydesignApp')
.controller('occasionCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {
    $scope.back = function() { 
        window.history.back();
     };

     $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };
        
    $http.get(__env.apiUrl + '/occasions')
    .success(function(data, status, headers, config) {
        $rootScope.img_path = __env.apiUrl + '/../static/';
        $scope.occasions = data;
    })
    .error(function(data, status, headers, config) {
        $scope.showError = true;
        $scope.message = 'Oops! Something went wrong!';
        $('#confirmation-window').show(); 
    });
    
    $scope.chooseOccasion = function(occasion){
        $rootScope.occasion = occasion;
        $rootScope.category = { id :0 };
        $location.path('/chooseclothes');
    };

}]);
