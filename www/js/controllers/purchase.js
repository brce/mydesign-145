angular.module('mydesignApp')
.controller('purchaseCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {
    $scope.back = function() { 
        window.history.back();
     };

    $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

    $scope.redirect = function (page) {
        $rootScope.page = page;
        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#confirmation-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#confirmation-mask').fadeIn(1000);
        $('#confirmation-mask').fadeTo("slow", 0.8);

        $('#confirmation-window').show();
    };
        
    $http.get( __env.apiUrl + '/purchases/user/' + $rootScope.user.id)
    .success(function(data, status, headers, config) {
        $scope.purchase = data;
        $scope.purchase.forEach(function(item) {
            item.cart = JSON.parse(item.cart);
        });
        if ($scope.purchase.length === 0) {
            $scope.showError = true;
            $scope.message = 'No purchases yet!';
            $('#confirmation-window').show();
        }
    })
    .error(function(data, status, headers, config) {
        $scope.showError = true;
        $scope.message = 'No purchases yet!';
        $('#confirmation-window').show(); 
    });
}]);
